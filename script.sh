#!/bin/bash
# Author: Baptiste DOUBLET <baptiste.doublet@cfa-afti.fr>

#######
# Usage function
#######
usage()
{
	name=$(basename "$0")
	cat >&2 <<-EOF
Usage: $name
  "OPTION:"
    "username: -u | --username [USERNAME]  -- Add an username to the host"
    "vhost:    -v | --vhost [VHOSTNAME]    -- Configure all vhost specified for the username"
    "help:     -h | --help                 -- Print the usage of create_hosting_account command"

EOF
	exit 2
}

if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi

if [[ $# -eq 0 ]]; then
    usage
fi

#######
# Parsing line command
#######
while [[ $# -gt 0 ]]
do
key="$1"

case $key in
    -u|--username )
        if [ -z $USERNAME ]; then
            USERNAME="$2"
            echo "USERNAME: $USERNAME"
            shift # past argument
            shift # past value
        else
            echo "[ERROR] Only one username at a time can be set up"
            usage
        fi
        ;;
    -v|--vhost)
        VHOST+=("$2")
        echo "VHOST: $VHOST"
        shift
        shift
        ;;
    -h|--help)
        usage
        ;;
    *)
        usage
        ;;
esac
done

for host in "${VHOST[@]}"
do
    echo "[INFO] Going to setup ${host}"
    echo ""
done

source ./script.conf

#######
# Install all packages
#######
apt install -y aptitude

if [ $(dpkg-query -W -f='${Status}' curl 2>/dev/null | grep -c "ok installed") -eq 0 ]; then
    echo "Can't find curl. Trying install it..."
    aptitude -y install curl
fi

if [ $(dpkg-query -W -f='${Status}' postgresql 2>/dev/null | grep -c "ok installed") -eq 0 ]; then
    echo "Can't find postgresql. Trying install it..."
    aptitude -y install postgresql
fi

if [ $(dpkg-query -W -f='${Status}' apache2 2>/dev/null | grep -c "ok installed") -eq 0 ]; then
    echo "Can't find apache2. Trying install it..."
    aptitude -y install apache2
fi

if [ $(dpkg-query -W -f='${Status}' php 2>/dev/null | grep -c "ok installed") -eq 0 ]; then
    echo "Can't find php. Trying install it..."
    aptitude -y install php php7.3-fpm libapache2-mod-php
fi

if [ $(dpkg-query -W -f='${Status}' vsftpd 2>/dev/null | grep -c "ok installed") -eq 0 ]; then
    echo "Can't find vsftpd. Trying install it..."
    aptitude -y install vsftpd
fi

source ./script.conf

#######
# ADD USER
#######
useradd -g www-data -p $APACHE_PASSWORD $USERNAME
groupadd $USERNAME
chage -W 10 -M 90 $USERNAME
echo "[INFO] New Account settings: $(chage -l ${USERNAME})"
echo ""

#######
# POSTGRESQL
#######
echo "[INFO] Install Postgresql"
echo ""

chage -W 10 -M 90 postgres
echo "[INFO] postgres account settings: $(chage -l ${USERNAME})"
echo ""
service postgresql restart

### pg_hba authorize peer connection of user postgres so I can connect through the system account postgres
## the user example has all privileges on exampledb except it's not the owner
echo "[INFO] Create postgresql ${USERNAME} User Account"
sudo -u postgres psql -c "CREATE USER ${USERNAME} WITH PASSWORD '${PSQL_PASSWORD}' NOSUPERUSER NOCREATEROLE NOCREATEDB LOGIN;"
echo "[INFO] Create the ${USERNAME}_db Database"
sudo -u postgres psql -c "CREATE DATABASE ${USERNAME}_db"
echo "[INFO] Grant all privileges ${USERNAME}_db Database;"
echo ""
sudo -u postgres psql -c "grant all privileges on database ${USERNAME}_db to ${USERNAME};"

#######
# APACHE2
#######
echo "[INFO] Install Apache Server"
echo ""

### Setup user password account
echo "[INFO] AddGroup www-data"

grep -q "www-data" /etc/group
if [ $? -eq 0 ]; then
    echo "[INFO] group www-data exists"
    echo ""
else
    echo "[ERROR] group www-data does not exist"
    echo ""
    groupadd www-data
fi

for host in "${VHOST[@]}"
do
    echo "[INFO] Create index.html in /var/www/${host}"
    mkdir -p /var/www/$host
    cat >/var/www/$host/index.html <<EOF
<html>
  <head>
    <title>Welcome to ${host}!</title>
  </head>
  <body> <h1>Success! The ${host} virtual host is working!</h1>
  </body>
</html>
EOF
    echo "[INFO] Create index.php in /var/www/${host}"
    cat >/var/www/$host/index.php <<EOF
<?php
echo "${host}";
phpinfo();
?>
EOF
    chown -R ${USERNAME}: /var/www/$host
    if [ ! -f /etc/apache2/sites-available/$host.conf ]; then
        touch /etc/apache2/sites-available/$host.conf
    else
        echo "[WARN] Configuration file /etc/apache2/sites-available/${host}.conf already exist"
        echo "" 
    fi

    echo "[INFO] Create /etc/apache2/sites-available/${host}.conf in /var/www/${host}"
    export HOSTNAME=$host
    export SOCKET=$(echo "${host}" | cut -d '.' -f1)
    envsubst < template/virtualhost.conf > /etc/apache2/sites-available/${host}.conf

### Add a local dns entry to resolve the host (only for the project)
	echo "[WARN] Add host in /etc/hosts (for the purpose of the project only)"
    echo ""

	grep -q "127.0.0.1 ${host}" /etc/hosts

	if [ $? -eq 0 ]; then
		echo "[INFO] Dns already created"
        echo ""
	else
        echo "127.0.0.1 ${host}" >> /etc/hosts
        echo ""
	fi
    
    a2ensite $host
done

#######
# PHP-FPM
#######
echo "[INFO] Install PHP-FPM"

echo "[INFO] Remove default pool"
rm -rf /etc/php/7.3/fpm/pool.d/www.conf

export PHP_USER=$USERNAME
for host in "${VHOST[@]}"
do
    echo "[INFO] Create pool php for ${host}"
    export HOSTNAME=$host
    export SOCKET=$(echo "${host}" | cut -d '.' -f1)
    envsubst < template/pool.conf > /etc/php/7.3/fpm/pool.d/${host}.conf
done

[ ! -d /var/run/ ] && mkdir -p /var/run/
echo "[INFO] Disable default Apache vhost configuration"
a2dissite 000-default
echo "[INFO] proxy_fcgi module"
a2enmod proxy_fcgi

echo "[INFO] Restart php7.3-fpm and apache server"
php-fpm7.3 -t
service php7.3-fpm restart
service apache2 restart

#######
# FTP
#######
grep -q "${USERNAME}" /etc/vsftpd.userlist
if [ $? -eq 0 ]; then
	echo "[INFO] ftp user already exists"
else
    echo "${USERNAME}" >> /etc/vsftpd.userlist
fi

echo "[INFO] Copy vsftpd configuration file"
cp template/vsftpd.conf /etc/vsftpd.conf
echo "[INFO] Starting vsftpd service"
service vsftpd start

for host in "${VHOST[@]}"
do
    echo "[INFO] Display index.html for vhost ${host}"
    curl $host
    echo ""
done

echo "FINISH INSTALLATION"
echo "[INFO] Display php pools"
ps -aux | grep php
echo ""
echo "[INFO] New Account settings for ${USERNAME}: $(chage -l ${USERNAME})"
echo ""
sudo -u postgres psql -c "\l"