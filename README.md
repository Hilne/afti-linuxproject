# Linux Project
This script deploys an apache2 server with php7.3-fpm, a Postgresql server and a FTP server with specified account

- [Linux Project](#linux-project)
  - [Requirements](#requirements)
  - [Group Variables](#group-variables)
  - [Usage](#usage)
  - [Run](#run)

## Requirements
  - Debian 10
## Variables

All variables which can be overridden are stored in [script.conf](script.conf) file and described in table below.

| Name             | Default Value        | Description                                                     |
| ---------------- | -------------------- | --------------------------------------------------------------- |
| `APACHE_LOG_DIR` | -                    | Define the directory of apache log         |
| `PSQL_PASSWORD`   | - | Define the password of postgresql Account for user defined |
| `APACHE_PASSWORD`   | -                 | Define the password of Apache Account for user defined    |

## Usage
```bash
Usage: script.sh
  "OPTION:"
    "username: -u | --username [USERNAME]  -- Add an username to the host"
    "vhost:    -v | --vhost [VHOSTNAME]    -- Configure all vhost specified for the username"
    "help:     -h | --help                 -- Print the usage of create_hosting_account command"

sudo ./script.sh -u <username> -v <vhost> -v <vhost> -v <vhost>
sudo ./script.sh -username <username> -v <vhost> --vhost <vhost>
```
## Run

```bash
import and run the virtualbox machine

THE KEYBAORD IS QWERTY !
log with login: linuxproject password: project

cd afti-linuxproject

sudo ./script.sh -u test -v example.org

### To check the virtualhost
curl example.org/index.html
curl example.org/index.php

### To check specific pool for vhost
ps -aux | grep php

### To check the database is correctly set with rights
sudo -u postgres psql -c "\l"
```